#include <functional>
#include <vector>
#include <string>
#include "display/lvgl.h"
#include "tinyxml2.h"
#include "functionconverter.h"

using namespace tinyxml2;
struct Binding {
  std::function<lv_res_t(lv_obj_t*)> func;
  std::string               key;

  Binding (std::function<lv_res_t(lv_obj_t*)> f, std::string keyin) {
    this->func = f;
    this->key  = keyin;
  }
};
std::function<lv_res_t(lv_obj_t*)> BSearch(std::vector<Binding> bindings, std::string n);
void BScreenDraw(XMLDocument* doc, std::vector<Binding> bindings);
