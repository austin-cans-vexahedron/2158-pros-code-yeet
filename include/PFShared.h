namespace Pathfinding {
  namespace AStar {
    //The structure for a square
    struct Square {
      char RiskLevel;
    };

    //The file will only contain occupied and unnocupied
    //SAFE means no obstacles within 12.75 inches. Point turning here should not cause a collision.
    //SMALL_RISK means obstacles within 12.75 inches, but no obstacles within 9 inches. Point turning 360 here will cause a collision. If the robot is oriented correctly, the robot can be here
    //LARGE_RISK means obstacles with 9 inches, but the square is not an obstacle. The field element is likely inside the robot. The Robot's center should never be here
    //OCCUPIED means the tile contains a field element. Congratulations, you centered your robot over an occupied square, and, in doing so, broke the field. I'm proud of you.
    enum SquareType {SAFE = 0, SMALL_RISK = 1, LARGE_RISK = 2, OCCUPIED = 3};

    //The array for squares and their risks is 166x166. This is to label the walls as occupied in order to make things good
    extern Square Obstacles [146] [146];


    void GenerateFieldPerimeter();

    void MergeObstacles(Square **Dest, Square **Source);
    void EvaluateRisks(Square (&FieldMap)[146][146]);
    unsigned char EvaluateIndividualRisk(Square (&FieldMap)[146][146], int x, int y);
    void DrawFieldMap(Square (&FieldMap)[146][146]);
  } //namespace AStar
} //namespace pathfinding
