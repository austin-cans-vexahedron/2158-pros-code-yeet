namespace Auto {

    namespace Lift {
        void MoveTo(double pos);
    }

    namespace Tray {
        void MoveTo(double pos);
        void Control();
        extern double Target;
    }

    namespace Intake {
        void SetSpeed(double spe);
    }
}