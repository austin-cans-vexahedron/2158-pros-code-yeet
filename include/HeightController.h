#include "MiniPID.h"
class HeightController {
private:
  MotorGroup *LMotors;
  QLength TLen;
  QLength BLen;
  QAngle  BAng;
  double  AngleRatio;
  MiniPID PIDController = MiniPID(1, 0, 0);
public:
  HeightController(MotorGroup &LiftMotors, QLength TopArmLength, QLength BottomArmLength, QAngle BottomArmAngleFromParallel, double ARatio);

  QLength CalculateCurrentHeight();

  double CalculateTargetRotation(QLength height);

  void GoToTarget(double tr);

  void GoToTarget(QLength height);
};
