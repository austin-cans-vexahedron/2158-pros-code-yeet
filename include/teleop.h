namespace Teleop {
	void DriveControl (void *CA);
	void LiftControl  (void *CA);
	void IntakeControl(void *CA);
	void TrayControl  (void *CA);
}
