#include "main.h"
#include "teleop.h"


void Teleop::IntakeControl (void *CA) {
	Controller *CT = static_cast< Controller* >(CA);
	IntakeMotorGroup.setBrakeMode(AbstractMotor::brakeMode::hold);
	bool kdown = false;
	while(true) {
		if(CT->getDigital(ControllerDigital::R1)) {
			IntakeMotorGroup.moveVelocity(200);

			if(!kdown && (TrayMotor.getPosition() > 100)) CT->rumble("-");
			kdown = true;
		} else if (CT->getDigital(ControllerDigital::R2)) {
			IntakeMotorGroup.moveVelocity(-200);
		} else {
			IntakeMotorGroup.moveVelocity(0);
			kdown = false;
		} //if(CT->getDigital(ControllerDigital::L1)) {
		pros::delay(20);
	}
}
