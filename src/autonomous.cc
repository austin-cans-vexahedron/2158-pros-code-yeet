#include "main.h"
#include <vector>
#include "AutoThreads.h"



/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */

void RedAuto() {
  Chassis->driveToPoint({1_ft, 1_ft});
  Chassis->waitUntilSettled();
}

void BlueAuto() {
  std::cout<<"Running blue autonomous"<<std::endl;
  IntakeMotorGroup.moveVelocity(-200);
  pros::delay(1000);
  IntakeMotorGroup.moveVelocity(200);
  ChassisMotionController->setTarget("match collect", false, false);
  ChassisMotionController->waitUntilSettled();

  ChassisMotionController->setTarget("match collect reverse", true, true);
  pros::delay(1000);
  IntakeMotorGroup.moveVelocity(0);
  ChassisMotionController->waitUntilSettled();

  Chassis->turnAngle(-135_deg);
  Chassis->waitUntilSettled();

  ChassisMotionController->setTarget("fwdplz");
  //ChassisMotionController->waitUntilSettled();

  IntakeMotorGroup.moveRelative(-650, 30);

  while(fabs(IntakeMotorGroup.getPositionError()) > 20) pros::delay(20);
  int h = 0;
  double eh =  1500 - TrayMotor.getPosition();
  for(int i = 0; i < 50 * 5; i++) {
    double Error = 1000 - TilterPot.get();
    TrayMotor.moveVelocity(.075 * Error < 200 ? .05 * Error : 200);

    if(Error < 20) h++;
    else h = 0;

    if((h > 20)|| ((Error < 400) && (fabs(Error - eh) < 5))) break;
    pros::delay(20);
  }
  IntakeMotorGroup.moveRelative(-100, 30);

  pros::delay(500);

  TrayMotor.moveAbsolute(0, 100);

  IntakeMotorGroup.moveVelocity(-100);
  ChassisMotionController->setTarget("fwdplz", true);
  ChassisMotionController->waitUntilSettled();
}



void SkillsAuto() {
  std::cout<<"Running skills autonomous"<<std::endl;
  
  IntakeMotorGroup.moveVelocity(-200);
  pros::delay(2000);
  IntakeMotorGroup.moveVelocity(200);
  TrayMotor.moveAbsolute(350, 200);
  ChassisMotionController->setTarget("collectabunchofcubes");
  pros::delay(1500);
  TrayMotor.moveAbsolute(0, 200);
  ChassisMotionController->waitUntilSettled();
  

  IntakeMotorGroup.moveVelocity(200);
  ChassisMotionController->setTarget("do the dab", true, true);
  ChassisMotionController->waitUntilSettled();
  ChassisMotionController->setTarget("do the thing", false, true);
  ChassisMotionController->waitUntilSettled();


  IntakeMotorGroup.moveRelative(-960/2, 60);
  while(fabs(IntakeMotorGroup.getPositionError()) > 20) pros::delay(20);
  int h = 0;
  double eh =  1140 - TilterPot.get();
  for(int i = 0; i < 50 * 7; i++) {
    double Error = 1140 - TilterPot.get();
    TrayMotor.moveVelocity(.075 * Error < 200 ? .04 * Error : 200);

    if(Error < 20) h++;
    else h = 0;

    if(Error < 400) {
      double ipwr = -100 * pow(sin(Error / 4), 2);
      IntakeMotorGroup.moveVelocity(ipwr);
    }

    if((h > 20)|| ((Error < 400) && (fabs(Error - eh) < 5))) break;
    pros::delay(20);
  }

  TrayMotor.moveVelocity(0);

  IntakeMotorGroup.moveVelocity(-30);
  pros::delay(500);
  IntakeMotorGroup.moveVelocity(0);

  pros::delay(1000);
  IntakeMotorGroup.moveVelocity(-60);
  
  
  ChassisMotionController->setTarget("do the thing", true, true);
  while(true) {
    double error = 440 - TilterPot.get();
    TrayMotor.moveVelocity(-50);

    if(TilterPot.get() < 440) {
      TrayMotor.moveVelocity(0);
      break;
    }
    pros::delay(20);
  }
  ChassisMotionController->waitUntilSettled();
  IntakeMotorGroup.moveVelocity(200);
  ChassisMotionController->setTarget("wumboing", false, false);
  ChassisMotionController->waitUntilSettled();
  pros::delay(1000);
  IntakeMotorGroup.moveVelocity(0);
  IntakeMotorGroup.moveRelative(-1260 * .6666, 200);
  ChassisMotionController->setTarget("fwdplz", true);
  ChassisMotionController->waitUntilSettled();

  LiftMotor.setEncoderUnits(AbstractMotor::encoderUnits::degrees);
  LiftMotor.tarePosition();
  LiftMotor.moveAbsolute(911, 100);
  pros::delay(1000);
  ChassisMotionController->setTarget("fwdplz", false);
  ChassisMotionController->waitUntilSettled();
  IntakeMotorGroup.moveVelocity(-200);

  ChassisMotionController->setTarget("reverse for cube after tower 1", true, true);
  ChassisMotionController->waitUntilSettled();
  IntakeMotorGroup.moveVelocity(200);
  LiftMotor.moveAbsolute(0, 200);

  ChassisMotionController->setTarget("reverse for cube after tower 1");
  ChassisMotionController->waitUntilSettled();

  ChassisMotionController->setTarget("reverse for cube after tower 1", true, true);
  ChassisMotionController->waitUntilSettled();

  ChassisMotionController->setTarget("reverse for cube after tower 1");
  ChassisMotionController->waitUntilSettled();

  IntakeMotorGroup.moveVelocity(0);
  IntakeMotorGroup.moveRelative(-1260 * .6666, 200);
  ChassisMotionController->setTarget("fwdplz");
  ChassisMotionController->waitUntilSettled();

  LiftMotor.setEncoderUnits(AbstractMotor::encoderUnits::degrees);
  LiftMotor.tarePosition();
  LiftMotor.moveAbsolute(820, 100);
  pros::delay(1000);

  ChassisMotionController->setTarget("forward before tower 2", false, true);
  ChassisMotionController->waitUntilSettled();

  IntakeMotorGroup.moveVelocity(-150);
}
AutonomousSelection asel;

extern pros::Task tray_auto;

void autonomous() {
  
  std::cout<<"Preparing autonomous"<<std::endl;
  std::cout<<"Running autonomous"<<std::endl;

  if(asel.iclr == 0) {}
  else if (asel.iclr == 1) RedAuto();
  else if (asel.iclr == 2) BlueAuto();
  else if (asel.iclr == 3) SkillsAuto();

  //After autonomous routines finish, lock up this thread with an infinite loop
  while(true) pros::delay(20);
}
