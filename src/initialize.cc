#include "main.h"
#include "init.h"
#include "PageBuilder.h"
#include "cansimage.h"

//#define NUM_SCRIPTS 5										// Number of autonomous scripts

// array fo scripts descriptions/names displayed in selection menu
const char* titles[] = {"Skills", "autoRedLeft", "AutoBlueLeft", "autoRedRight", "autoBlueRight"};

int selection;													// Select script to run
unsigned int scriptNumber = 0;					// scriptNumber which will be passed
																				// to autonomous() and run matching function
																			  // via hte glabal selection

static bool selectionMade = false;

LV_IMG_DECLARE(cansimage)





/**
 * Runs initialization code. This occurs as soon as the program is started.
 *
 * All other competition modes are blocked by initialize; it is recommended
 * to keep execution time for this mode under a few seconds.
 */

/**
 * Runs while the robot is in the disabled state of Field Management System or
 * the VEX Competition Switch, following either autonomous or opcontrol. When
 * the robot is enabled, this task will exit.
 */
void disabled() {
}

lv_obj_t * auto_red_btn;
lv_obj_t * auto_blu_btn;
lv_obj_t * auto_skl_btn;
lv_obj_t * tabUC;


void btnSetToggled(lv_obj_t * btn, bool toggled)
{
	if (toggled != (lv_btn_get_state(btn) >= 2)) lv_btn_toggle(btn);
}
#include <iostream>
lv_res_t clickEvent(lv_obj_t * btn) {
	btnSetToggled(auto_red_btn, btn == auto_red_btn);
	btnSetToggled(auto_blu_btn, btn == auto_blu_btn);
	btnSetToggled(auto_skl_btn, btn == auto_skl_btn);

  if(btn == auto_red_btn) asel.iclr = AutonomousSelection::Red;
  if(btn == auto_blu_btn) asel.iclr = AutonomousSelection::Blue;
  if(btn == auto_skl_btn) asel.iclr = AutonomousSelection::Skills;
  std::cout<<"Set autonomous "<<asel.iclr<<std::endl;
	return LV_RES_OK;
}

void menu_create(void) {
	lv_coord_t hres = 480;
	lv_coord_t vres = 240;


	static lv_style_t style_tv_btn_bg;
	lv_style_copy(&style_tv_btn_bg, &lv_style_plain);
	style_tv_btn_bg.body.main_color = lv_color_hex(0x487fb7);
	style_tv_btn_bg.body.grad_color = lv_color_hex(0x487fb7);

	static lv_style_t style_tv_btn_rel;
	lv_style_copy(&style_tv_btn_rel, &lv_style_btn_rel);
	style_tv_btn_rel.body.opa = LV_OPA_TRANSP;
	style_tv_btn_rel.body.border.width = 0;

	static lv_style_t style_tv_btn_pr;
	lv_style_copy(&style_tv_btn_pr, &lv_style_btn_pr);
	style_tv_btn_pr.body.radius = 0;
	style_tv_btn_pr.body.opa = LV_OPA_50;
	style_tv_btn_pr.body.main_color = lv_color_hex(0xffffff);
	style_tv_btn_pr.body.grad_color = lv_color_hex(0xffffff);
	style_tv_btn_pr.body.border.width = 0;
	style_tv_btn_pr.text.color = lv_color_hex(0x222222);

	lv_obj_t * tv = lv_tabview_create(lv_scr_act(), NULL);
	lv_obj_set_size(tv, hres, vres);
	lv_tabview_set_btns_pos(tv, LV_TABVIEW_BTNS_POS_BOTTOM);

	//Add tabs to the tab thing
	lv_obj_t * tabHome = lv_tabview_add_tab(tv, "Home");
	lv_obj_t * tabAuto = lv_tabview_add_tab(tv, "Auto");
	tabUC = lv_tabview_add_tab(tv, "Driver");

	//Set styles for the tabview
	lv_tabview_set_style(tv, LV_TABVIEW_STYLE_BTN_BG, &style_tv_btn_bg);
	lv_tabview_set_style(tv, LV_TABVIEW_STYLE_INDIC, &lv_style_plain);
	lv_tabview_set_style(tv, LV_TABVIEW_STYLE_BTN_REL, &style_tv_btn_rel);
	lv_tabview_set_style(tv, LV_TABVIEW_STYLE_BTN_PR, &style_tv_btn_pr);
	lv_tabview_set_style(tv, LV_TABVIEW_STYLE_BTN_TGL_REL, &style_tv_btn_rel);
	lv_tabview_set_style(tv, LV_TABVIEW_STYLE_BTN_TGL_PR, &style_tv_btn_pr);

	//Red button style
	static lv_style_t style_btn_red;
	lv_style_copy(&style_btn_red, &lv_style_plain);
	style_btn_red.body.main_color = lv_color_hex(0xff0000);
	style_btn_red.body.grad_color = lv_color_hex(0xff0000);
	style_btn_red.text.color = lv_color_hex(0xffffff);

	//Blue Button style
	static lv_style_t style_btn_blue;
	lv_style_copy(&style_btn_blue, &lv_style_plain);
	style_btn_blue.body.main_color = lv_color_hex(0x0000ff);
	style_btn_blue.body.grad_color = lv_color_hex(0x0000ff);
	style_btn_blue.text.color = lv_color_hex(0xffffff);

	//Gray Button style
	static lv_style_t style_btn_gray;
	lv_style_copy(&style_btn_gray, &lv_style_plain);
	style_btn_gray.body.main_color = lv_color_hex(0x888888);
	style_btn_gray.body.grad_color = lv_color_hex(0x888888);

	//Green Button style
	static lv_style_t style_btn_green;
	lv_style_copy(&style_btn_green, &style_btn_blue);
	style_btn_green.body.main_color = lv_color_hex(0x00ff00);
	style_btn_green.body.grad_color = lv_color_hex(0x00ff00);

	auto_red_btn = lv_btn_create(tabAuto, NULL);
	lv_btn_set_toggle(auto_red_btn, true);
	lv_btn_set_style(auto_red_btn, LV_BTN_STYLE_TGL_REL,  &style_btn_red);
	lv_btn_set_style(auto_red_btn, LV_BTN_STYLE_REL, &style_btn_gray);
	lv_btn_set_style(auto_red_btn, LV_BTN_STYLE_TGL_PR, &style_btn_red);
	lv_btn_set_style(auto_red_btn, LV_BTN_STYLE_PR, &style_btn_gray);
	lv_btn_set_action(auto_red_btn, LV_BTN_ACTION_CLICK, clickEvent);
	lv_obj_set_pos(auto_red_btn, 20, 20);
	lv_obj_set_size(auto_red_btn, 50, 50);
	lv_obj_t * auto_red_label = lv_label_create(auto_red_btn, NULL);
	lv_label_set_text(auto_red_label, "RED");

	auto_blu_btn = lv_btn_create(tabAuto, NULL);
	lv_btn_set_toggle(auto_blu_btn, true);
	lv_btn_set_style(auto_blu_btn, LV_BTN_STYLE_TGL_REL, &style_btn_blue);
	lv_btn_set_style(auto_blu_btn, LV_BTN_STYLE_REL, &style_btn_gray);
	lv_btn_set_style(auto_blu_btn, LV_BTN_STYLE_TGL_PR, &style_btn_blue);
	lv_btn_set_style(auto_blu_btn, LV_BTN_STYLE_PR, &style_btn_gray);
	lv_btn_set_action(auto_blu_btn, LV_BTN_ACTION_CLICK, clickEvent);
	lv_obj_set_pos(auto_blu_btn, 100, 20);
	lv_obj_set_size(auto_blu_btn, 50, 50);
	lv_obj_t * auto_blu_label = lv_label_create(auto_blu_btn, NULL);
	lv_label_set_text(auto_blu_label, "BLUE");

	auto_skl_btn = lv_btn_create(tabAuto, NULL);
	lv_btn_set_toggle(auto_skl_btn, true);
	lv_btn_set_style(auto_skl_btn, LV_BTN_STYLE_TGL_REL, &style_btn_green);
	lv_btn_set_style(auto_skl_btn, LV_BTN_STYLE_REL, &style_btn_gray);
	lv_btn_set_style(auto_skl_btn, LV_BTN_STYLE_TGL_PR, &style_btn_green);
	lv_btn_set_style(auto_skl_btn, LV_BTN_STYLE_PR, &style_btn_gray);
	lv_btn_set_action(auto_skl_btn, LV_BTN_ACTION_CLICK, clickEvent);
	lv_obj_set_pos(auto_skl_btn, 180, 20);
	lv_obj_set_size(auto_skl_btn, 50, 50);
	lv_obj_t * auto_skl_label = lv_label_create(auto_skl_btn, NULL);
	lv_label_set_text(auto_skl_label, "SKL");

	lv_obj_t * canman = lv_img_create(tabHome, NULL);
	lv_img_set_src (canman, &cansimage);
	lv_obj_align(canman, NULL, LV_ALIGN_CENTER, -100, -10);

	static lv_style_t style_yellow;
	lv_style_copy(&style_yellow, &lv_style_plain);
	style_yellow.body.main_color = lv_color_hex(0xffcf46);
	style_yellow.body.grad_color = lv_color_hex(0xffcf46);
	style_yellow.text.color = lv_color_hex(0xffffff);

	lv_page_set_style(tabHome, LV_PAGE_STYLE_BG, &style_yellow);
}
 //comment for git test, ignore me
#include "AutoThreads.h"

#define DEFAULT_AUTONOMOUS auto_red_btn

void initialize() {
  //Initialize the drive base and motion profile controller

  
  Chassis = ChassisControllerBuilder()
    .withMotors (
      DriveLeft,
      DriveRight
    )
	/*.withGains(
		{1 , 0 ,.0},
		{.0135, 0, .000544}
	)*/
    .withDimensions(
	  AbstractMotor::gearset::blue,
      {{3.25_in, 13.75_in}, imev5BlueTPR * (5.0 / 3.0)}
    )
	.withSensors(
		okapi::ADIEncoder{'A', 'B'},
		okapi::ADIEncoder{'C', 'D'},
		okapi::ADIEncoder{'E', 'F'}
	)
	.withOdometry({{2.75_in, 6.25_in, 4.65_in, 2.75_in}, quadEncoderTPR})
	.buildOdometry()
  ;
  std::cout<<"Built drivebase"<<std::endl;

  ChassisMotionController = AsyncMotionProfileControllerBuilder()
    .withOutput(Chassis->getModel(), {{3.25_in, 11.25_in}, imev5BlueTPR * (5.0 / 3.0)}, {AbstractMotor::gearset::blue, (5.0 / 3.0)})
    .withLimits({.3, .41, 1})
    .buildMotionProfileController()
  ;

  std::cout<<"Built 2d profiler"<<std::endl;

  std::cout<<"Built 1d profiler"<<std::endl;
  

  LiftController = AsyncPosControllerBuilder()
	.withMotor(LiftMotor)
	.build()
  ;

  /*TrayController = AsyncPosControllerBuilder()
	.withMotor(TrayMotor)
	.build()
  ;*/

  

  std::cout<<"Built chassis profile controller"<<std::endl;

  PathfinderPoint p0 = {0_in, 0_in, 0_deg};
  PathfinderPoint p1 = {2_ft, 0_in, 0_deg};
  ChassisMotionController->generatePath({p0, p1}, "path1");
  std::cout<<"Generated path1 for Chassis"<<std::endl;

  ChassisMotionController->generatePath(
	  {
		  {-7_in, 0_in, 0_deg},
		  {2.5_ft, 0_in, 0_deg},
		  {5_ft, -2_in, 0_deg},
		  {7.0_ft, 0_in, 0_deg},
		  {8.9_ft, 0_in, 0_deg}
	  }, "collectabunchofcubes"
  );

  ChassisMotionController->generatePath(
	  {
		  {0_ft, 0_in, 0_deg},
	  	{.55_ft, 0_in, 0_deg}
	  }, "do the dab"
  );

  ChassisMotionController->generatePath(
	  {
		  {7.7_ft, 0_in, 0_deg},
	  	{9.7_ft, 1.75_ft, 45_deg}
	  }, "do the thing"
  );

  ChassisMotionController->generatePath(
	  {
		  {0_in, 0_in, 0_deg},
		  {1.35_ft, 3_ft, 100_deg}
	  }, "wumboing"
  );

	
  ChassisMotionController->generatePath(
	  {
		  {0_in, 0_in, 0_deg},
		  {3_ft, 0_in, 0_deg}
	  }, "match collect", {.5, .41, 10}
  );

  /*,
		  {2_ft, -1.5_ft, -180_deg},
		  {1_ft, -3_ft, -135_deg}*/

  ChassisMotionController->generatePath(
	  {
		  {0_in, 0_in, 0_deg},
		  {1.3_ft, 0_ft, 0_deg}
	  }, "match collect reverse", {1, .65, 10}
  );

  ChassisMotionController->generatePath(
	  {
		  {0_in, 0_in, 0_deg},
		  {18_in, 0_in, 0_deg}
	  }, "fwdplz", {.8, .61, 10}
  );

  ChassisMotionController->generatePath(
	  {
		  {0_in, 0_in, 0_deg},
		  {8_in, 5_in, 42_deg}
	  }, "reverse for cube after tower 1", {.2, .21, 10}
  );

  ChassisMotionController->generatePath(
	  {
		  {0_in, 0_in, 0_deg},
		  {5_in, 3_in, 15_deg}
	  }, "forward before tower 2", {.2, .21, 10}
  );

  
  
  std::cout<<"Generated path collectabunchofcubes for Chassis"<<std::endl;

  //Put the tray motor into degree mode
  TrayMotor.setGearing(AbstractMotor::gearset::red);
  TrayMotor.setEncoderUnits(AbstractMotor::encoderUnits::degrees);

  //Same for liftmotor
  LiftMotor.setGearing(AbstractMotor::gearset::red);

  //Create the menu
  menu_create();

  clickEvent( DEFAULT_AUTONOMOUS );
}
void competition_initialize() {

}
