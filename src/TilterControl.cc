#include "main.h"
#include "teleop.h"

double kP = .1;
double trayLimit = 1190;



void Teleop::TrayControl (void *CA) {
	Controller *CT = static_cast< Controller* >(CA);
	TrayMotor.setBrakeMode(AbstractMotor::brakeMode::brake);
	while(true) {
		if(CT->getDigital(ControllerDigital::L1)) {
			TrayMotor.moveVelocity(
				160
			);
			
		} else if (CT->getDigital(ControllerDigital::L2)) {
			TrayMotor.moveVelocity(-160);
		} else {
			TrayMotor.moveVelocity(0);
		} //if(CT->getDigital(ControllerDigital::L1)) {
		pros::delay(20);
	}
}
