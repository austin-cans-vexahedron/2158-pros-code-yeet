#include "main.h"
#include "teleop.h"

extern lv_obj_t * tabUC;

void Teleop::DriveControl (void *CA) {
	Controller *CT = static_cast< Controller* >(CA);

	lv_obj_t * encstats = lv_label_create(tabUC, NULL);
	lv_obj_set_pos(encstats, 0, 105);

	lv_obj_t * posstats = lv_label_create(tabUC, NULL);
	lv_obj_set_pos(posstats, 0, 120);

	/*okapi::ADIEncoder ab {'A', 'B'};
	okapi::ADIEncoder cd {'C', 'D', true};
	okapi::ADIEncoder ef {'E', 'F'};*/

	while(true) {

		char ecs[512];
		sprintf(ecs, "L:%d, R:%d, S:%d", Chassis->getOdometry()->getModel()->getSensorVals()[0], Chassis->getOdometry()->getModel()->getSensorVals()[1], Chassis->getOdometry()->getModel()->getSensorVals()[2]);

		char pss[512];
		sprintf(pss, "X:%f, Y:%f, Z:%f", Chassis->getOdometry()->getState().x.convert(okapi::inch), Chassis->getOdometry()->getState().y.convert(okapi::inch), Chassis->getOdometry()->getState().theta.convert(okapi::degree));


		lv_label_set_text(encstats, ecs);
		lv_label_set_text(posstats, pss);

		Chassis->getModel()->arcade(
			CT->getAnalog(ControllerAnalog::leftY),
			CT->getAnalog(ControllerAnalog::rightX)
		);
		pros::delay(20);
	} // while (true) {
}
