#include "main.h"



Motor LiftMotor = -15;
Motor TrayMotor = Motor(4);



MotorGroup IntakeMotorGroup({-20, 7});

//Drive motors (md<side><number>)
Motor mdl1 = -11;
Motor mdl2 = -19;
Motor mdr1 = 3;
Motor mdr2 = 14;

MotorGroup DriveLeft({mdl1,mdl2});
MotorGroup DriveRight({mdr1, mdr2});
CCPTR Chassis;
std::shared_ptr<AsyncMotionProfileController> ChassisMotionController;
std::shared_ptr<AsyncLinearMotionProfileController> ChassisLinearController;
Controller ControllerMain;

std::shared_ptr<AsyncPositionController<double,double>> LiftController;
std::shared_ptr<AsyncPositionController<double,double>> TrayController;

Potentiometer TilterPot ('A');



/*/Motion profiler for the drive
AsyncMotionProfileController profileController = AsyncControllerFactory::motionProfile(
  .8,     //Max speed
  1.75,   //Max acceleration
  15.0,   //Max jerk
  DModel,
  {3.25, 15},
  AbstractMotor::gearset::green
);*/
