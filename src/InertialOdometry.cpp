#include "okapi/api.hpp"

using namespace okapi;
typedef QSpeed QVelocity;


template<typename T, short x, short y>
struct Matrix {
    T data [x][y];

    
};

template<typename T, short x1, short y1, short x2, short y2>
Matrix<T, x2, y1> operator*( Matrix<T,x1,y1> i1, Matrix<T,x2,y2> i2 ) {

}

class InertialOdometry : okapi::Odometry {

    //Based on https://arxiv.org/pdf/1703.00154.pdf
    struct {

        //Position
        struct {
            QLength x;
            QLength y;
            QLength z;
        } p;

        //Velocity
        struct {
            QVelocity x;
            QVelocity y;
            QVelocity z;
        } v;

        //Orientation
        pros::c::quaternion_s q;

        //Accelerometer bias
        struct {
            QAcceleration x;
            QAcceleration y;
            QAcceleration z;
        } bias_a;

        //Gyroscope bias
        struct {
            QAngularAcceleration x;
            QAngularAcceleration y;
            QAngularAcceleration z;
        } bias_w;

        //Multiplicative accelerometer scale error

        struct {

        } T;
    } state;

    
};