#include "main.h"
#include "teleop.h"


//vytytytyoid copysign(){}
//CA is a void pointer to a Controller in memory
//CT is a Controller* pointer to a Controller in memory
//CT->(something) dereferences and gets that member
extern pros::Task tray_uc;

void Warn() {
	pros::delay(75000);
	ControllerMain.rumble(".. ..");
}
void opcontrol() {
	printf("%p",(void*)&ControllerMain);
	pros::Task T_Drive(Teleop::DriveControl, &ControllerMain);
	pros::Task T_Lift (Teleop::LiftControl , &ControllerMain);
	pros::Task T_Intk (Teleop::IntakeControl, &ControllerMain);
	pros::Task T_Tray (Teleop::TrayControl, &ControllerMain);

	pros::Task T_Warn (Warn, 72);

	threads.insert(threads.end(), &T_Drive);
	threads.insert(threads.end(), &T_Lift);
	threads.insert(threads.end(), &T_Intk);
	threads.insert(threads.end(), &T_Tray);

	while(true) {
		pros::delay(20);
	}
}
