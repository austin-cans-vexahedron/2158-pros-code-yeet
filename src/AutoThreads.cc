#include "main.h"
#include "AutoThreads.h"

namespace Auto {
    namespace Tray {
        double Target = 0;
    }
}

void Auto::Lift::MoveTo(double pos) {
    LiftMotor.moveAbsolute(pos, 100);
}

void Auto::Tray::MoveTo(double pos) {
    Target = pos;
}

void Auto::Intake::SetSpeed(double spe) {
    IntakeMotorGroup.moveVelocity(spe);
}

