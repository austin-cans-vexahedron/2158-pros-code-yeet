#include "main.h"
#include "teleop.h"

#define LOW_TARGET 820
#define HIGH_TARGET 911

void Teleop::LiftControl (void *CA) {
	Controller *CT = static_cast< Controller* >(CA);
	LiftMotor.setBrakeMode(AbstractMotor::brakeMode::hold);
	LiftMotor.setEncoderUnits(AbstractMotor::encoderUnits::degrees);
	double target = -1;
	while(true) {
		if(CT->getDigital(ControllerDigital::up)) {
			LiftMotor.moveVelocity(160);
			target = -1;
		} else if (CT->getDigital(ControllerDigital::down)) {
			LiftMotor.moveVelocity(-160);
			target = -1;
		} else {

			if(CT->getDigital(ControllerDigital::left)) target = LOW_TARGET;
			if(CT->getDigital(ControllerDigital::right)) target = HIGH_TARGET;
			if(target > 0) 
				LiftController->setTarget(target);

			if(LiftController->isSettled() || (target < 0)) LiftMotor.moveVelocity(0);
			target = -1;
		} //if(CT->getDigital(ControllerDigital::L1)) {
		pros::delay(20);
	}
}
